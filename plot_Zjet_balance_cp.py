#!/usr/bin/env python

import sys
from ROOT import *
from array import array

gROOT.Macro( "rootlogon.C" )
gROOT.LoadMacro( "AtlasUtils.C" )
gROOT.SetBatch(1)

#########################################################

def MakeCanvas( npads = 1, side = 800, split = 0.25, padding = 0.00 ):
    # assume that if pads=1 => square plot
    # padding used to be 0.05
    y_plot    = side * ( 1. - ( split + padding ) )
    y_ratio   = side * split
    y_padding = side * padding

    height_tot = y_plot + npads * ( y_ratio + y_padding )
    height_tot = int(height_tot)

    c = TCanvas( "PredictionData", "Prediction/Data", side, height_tot )
    c.SetFrameFillStyle(4000)
    c.SetFillColor(0)

    pad0 = TPad( "pad0","pad0",0, split+padding,1,1,0,0,0 )
    pad0.SetLeftMargin( 0.18 ) #0.16
    pad0.SetRightMargin( 0.05 )
    pad0.SetBottomMargin( 0. )
    #pad0.SetTopMargin( 0.14 )
    pad0.SetTopMargin( 0.07 ) #0.05
    pad0.SetFillColor(0)
    pad0.SetFillStyle(4000)
    pad0.Draw()

    pad1 = TPad( "pad1","pad1",0,0,1, split,0,0,0 )
    pad1.SetLeftMargin( 0.18 ) #0.16
    pad1.SetRightMargin( 0.05 )
    pad1.SetTopMargin( 0. )
#    pad1.SetBottomMargin( 0. )
    pad1.SetGridy(1)
    pad1.SetTopMargin(0)
    pad1.SetBottomMargin(0.5) #0.4
    pad1.Draw()
    pad1.SetFillColor(0)
    pad1.SetFillStyle(4000)

    pad0.cd()
    return c, pad0, pad1

#########################################################

def MakeLegend( params ):
    leg = TLegend( params['xoffset'], params['yoffset'], params['xoffset'] + params['width'], params['yoffset'] )
    leg.SetNColumns(1)
    leg.SetFillColor(0)
    leg.SetFillStyle(0)
    leg.SetBorderSize(0)
    leg.SetTextFont(72)
    leg.SetTextSize(0.04)#0.05)
    return leg


#########################################################

def SetTH1FStyle( h, color = kBlack, linewidth = 1, linestyle=1, fillcolor = 0, fillstyle = 0, markerstyle = 21, markersize = 1.3, fill_alpha=0.0 ):
    '''Set the style with a long list of parameters'''

    h.SetLineColor( color )
    h.SetLineWidth( linewidth )
    h.SetFillColor( fillcolor )
    h.SetFillStyle( fillstyle )
    h.SetLineStyle( linestyle )
    h.SetMarkerStyle( markerstyle )
    h.SetMarkerColor( h.GetLineColor() )
    h.SetMarkerSize( markersize )
    if fill_alpha > 0:
       h.SetFillColorAlpha( color, fill_alpha )


#########################################################

def DivideBy( h, h_ref ):
  h_div = h.Clone( "%s_div"%h.GetName() )
  h_div.Reset()

  nbins = h_ref.GetNbinsX()
  for i in range(nbins):
     y_ref = h_ref.GetBinContent(i+1)
     y     = h.GetBinContent(i+1)
     dy    = h.GetBinError(i+1)

     if y_ref == 0.:
        continue

     h_div.SetBinContent( i+1, y/y_ref )
     h_div.SetBinError( i+1, dy/y_ref )

  return h_div

#########################################################

def TH1F2TGraph(h):
  g = TGraphErrors()
  g.SetName( "g_%s" % h.GetName() )

  Nbins = h.GetNbinsX()
  for i in range(Nbins):
    x = h.GetBinCenter( i+1 )
    y = h.GetBinContent( i+1 )
    bw = h.GetBinWidth( i+1 )
    dy = h.GetBinError( i+1 )
    dx = bw/2.

    g.SetPoint( i, x, y )
    g.SetPointError( i, dx, dy )

  return g

#########################################################
#region = "lpT20.jpT20"
#region = "lpT25.jpT20"
#region = "lpT25.jpT25"

production = sys.argv[1]
channel = sys.argv[2]
region = sys.argv[3]

#onlyZjets = False
#if production == "output_A110_tightLH":
#    onlyZjets = True
onlyZjets = True

#infilename_data = production+"/data.Zjet_balance."+region+".root"
#infilename_Zj   = production+"/mc.Zjets.Zjet_balance."+region+".root"
infilename_data = production+"/"+channel+"/data_JpT10GeV.Zjet_balance."+region+".root"
infilename_Zj   = production+"/"+channel+"/mc.Zjets_JpT10GeV.Zjet_balance."+region+".root"

if not onlyZjets:
    infilename_tt   = production+"/"+channel+"/mc.ttbar.Zjet_balance."+region+".root"


infile_data = TFile.Open( infilename_data )
infile_Zj   = TFile.Open( infilename_Zj )
if not onlyZjets:
    infile_tt   = TFile.Open( infilename_tt )

hname = "Z_m"
if len(sys.argv) > 1: hname = sys.argv[4]

h_data = infile_data.Get(hname)
h_Zj   = infile_Zj.Get(hname)
if not onlyZjets:
    h_tt   = infile_tt.Get(hname)

h_mc = h_Zj.Clone("h_mc")
if not onlyZjets:
    h_mc.Add( h_tt )
h_mc_unc = TH1F2TGraph( h_mc )

SetTH1FStyle( h_data, color=kBlack, markerstyle=20 )
SetTH1FStyle( h_Zj,   fillstyle=1001, fillcolor=909,  markersize=0, linewidth=0 )
if not onlyZjets:
    SetTH1FStyle( h_tt,   fillstyle=1001, fillcolor=868, markersize=0, linewidth=0 )
#SetTH1FStyle( h_Wj,   fillstyle=1001, fillcolor=kGreen-6, markersize=0, linewidth=0 )
SetTH1FStyle( h_mc_unc,  fillstyle=3354, fillcolor=kGray+3, markersize=0, linewidth=0 )

h_pred = THStack("MC", "MC")
#h_pred.Add( h_Wj )
if not onlyZjets:
    h_pred.Add( h_tt )
h_pred.Add( h_Zj )

c, pad0, pad1 = MakeCanvas()

pad0.cd()

h_pred.Draw( "h" )
h_pred.GetYaxis().SetTitle( "Events" )
hmax = max( [ h_mc.GetMaximum(), h_data.GetMaximum() ] )
h_pred.SetMaximum( 1.3*hmax )

h_mc_unc.Draw("e2 same")
h_data.Draw( "ep same" )

lparams = {
        'xoffset' : 0.60,
        'yoffset' : 0.90,
        'width'   : 0.35,
        'height'  : 0.04,
        }

leg = MakeLegend( lparams )
leg.SetTextFont( 42 )
leg.SetNColumns(1)
leg.AddEntry( h_data, "Data", "ep" )
if channel == "Zmumu":
    leg.AddEntry( h_Zj,   "Z#rightarrow #mu^{+}#mu^{-}", "f" )
if channel == "Zee":
    leg.AddEntry( h_Zj,   "Z#rightarrow e^{+}e^{-}", "f" )

if not onlyZjets:
    leg.AddEntry( h_tt,   "t#bar{t}", "f" )
leg.AddEntry( h_mc_unc, "Uncertainty", "f" )
leg.Draw()
leg.SetY1( leg.GetY1() - lparams['height'] * leg.GetNRows() )

txt = TLatex()
txt.SetTextFont(42)
txt.SetNDC()
txt.SetTextSize(0.05)
txt.DrawLatex(0.25, 0.85, "ATLAS Internal" )
txt.SetTextSize(0.04)
txt.DrawLatex(0.25, 0.80, "#sqrt{s} = 5 TeV, 0.26 fb^{-1}" )
if region == "lpT20.jpT10":
    txt.DrawLatex(0.25, 0.75, "l_pT>20, j_pT>10" )
if region == "lpT20.jpT20":
    txt.DrawLatex(0.25, 0.75, "l_pT>20, j_pT>20" )
if region == "lpT25.jpT20":
    txt.DrawLatex(0.25, 0.75, "l_pT>25, j_pT>20" )
if region == "lpT25.jpT25":
    txt.DrawLatex(0.25, 0.75, "l_pT>25, j_pT>25" )


gPad.RedrawAxis()

if hname in [ "pt_ref" ]:
  h_pred.SetMaximum( 100*h_pred.GetMaximum() )
  h_pred.SetMinimum(0.05 )
  gPad.SetLogy()

pad1.cd()

xtitle = h_data.GetXaxis().GetTitle()

h_ratio_data = DivideBy( h_data,  h_mc )
h_ratio_mc = DivideBy( h_mc,    h_mc )

g_unc   = TH1F2TGraph( h_ratio_mc )
g_data  = TH1F2TGraph( h_ratio_data )

SetTH1FStyle( g_unc,   fillstyle=1001, fillcolor=kGray+1, linewidth=0, markersize=0 )
SetTH1FStyle( g_data,  color=kBlack, linewidth=2, markerstyle=h_data.GetMarkerStyle() )

xmin = h_data.GetXaxis().GetXmin()
xmax = h_data.GetXaxis().GetXmax()
frame = gPad.DrawFrame( xmin, 0.4, xmax, 1.6 )
frame.GetXaxis().SetNdivisions(508)
frame.GetYaxis().SetNdivisions(504)
frame.GetXaxis().SetLabelSize( 0.16 )
frame.GetXaxis().SetTitleSize( 0.16 )
frame.GetXaxis().SetTitleOffset( 1.2 )
frame.GetYaxis().SetLabelSize( 0.16 )
frame.GetYaxis().SetTitle( "Data/Pred.   " )
frame.GetYaxis().SetTitleSize( 0.16 )
frame.GetYaxis().SetTitleOffset( 0.5 )
frame.GetXaxis().SetTitle( xtitle )

r_line = TLine()
r_line.SetLineStyle(kDashed)
r_line.SetLineWidth(2)

frame.Draw()
g_unc.Draw( "e2 same" )
r_line.DrawLine( xmin, 1.0, xmax, 1.0 )
g_data.Draw( "ep same" )

gPad.RedrawAxis()

c.cd()

c.SaveAs( "%s/%s/img/%s/%s_Zjets_balance.png" % (production,channel,region,hname) )
