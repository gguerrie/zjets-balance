#!/usr/bin/env python

import sys
from ROOT import *
from array import array

gROOT.Macro( "rootlogon.C" )
gROOT.LoadMacro( "AtlasUtils.C" )
gROOT.SetBatch(1)

#########################################################

def MakeCanvas( npads = 1, side = 800, split = 0.25, padding = 0.00 ):
    # assume that if pads=1 => square plot
    # padding used to be 0.05
    y_plot    = side * ( 1. - ( split + padding ) )
    y_ratio   = side * split
    y_padding = side * padding

    height_tot = y_plot + npads * ( y_ratio + y_padding )
    height_tot = int(height_tot)

    #c = TCanvas( "PredictionData", "Prediction/Data", side, height_tot )
    c = TCanvas( "DataPrediction", "Data/Prediction", side, height_tot )
    c.SetFrameFillStyle(4000)
    c.SetFillColor(0)

    pad0 = TPad( "pad0","pad0",0, split+padding,1,1,0,0,0 )
    pad0.SetLeftMargin( 0.18 ) #0.16
    pad0.SetRightMargin( 0.05 )
    pad0.SetBottomMargin( 0. )
    #pad0.SetTopMargin( 0.14 )
    pad0.SetTopMargin( 0.07 ) #0.05
    pad0.SetFillColor(0)
    pad0.SetFillStyle(4000)
    pad0.Draw()

    pad1 = TPad( "pad1","pad1",0,0,1, split,0,0,0 )
    pad1.SetLeftMargin( 0.18 ) #0.16
    pad1.SetRightMargin( 0.05 )
    pad1.SetTopMargin( 0. )
#    pad1.SetBottomMargin( 0. )
    pad1.SetGridy(1)
    pad1.SetTopMargin(0)
    pad1.SetBottomMargin(0.5) #0.4
    pad1.Draw()
    pad1.SetFillColor(0)
    pad1.SetFillStyle(4000)

    pad0.cd()
    return c, pad0, pad1

#########################################################

def MakeLegend( params ):
    leg = TLegend( params['xoffset'], params['yoffset'], params['xoffset'] + params['width'], params['yoffset'] )
    leg.SetNColumns(1)
    leg.SetFillColor(0)
    leg.SetFillStyle(0)
    leg.SetBorderSize(0)
    leg.SetTextFont(72)
    leg.SetTextSize(0.04)#0.05)
    return leg


#########################################################

def SetTH1FStyle( h, color = kBlack, linewidth = 1, linestyle=1, fillcolor = 0, fillstyle = 0, markerstyle = 21, markersize = 1.3, fill_alpha=0.0 ):
    '''Set the style with a long list of parameters'''

    h.SetLineColor( color )
    h.SetLineWidth( linewidth )
    h.SetFillColor( fillcolor )
    h.SetFillStyle( fillstyle )
    h.SetLineStyle( linestyle )
    h.SetMarkerStyle( markerstyle )
    h.SetMarkerColor( h.GetLineColor() )
    h.SetMarkerSize( markersize )
    if fill_alpha > 0:
       h.SetFillColorAlpha( color, fill_alpha )


#########################################################

def DivideBy( h, h_ref ):
  h_div = h.Clone( "%s_div"%h.GetName() )
  h_div.Reset()

  nbins = h_ref.GetNbinsX()
  for i in range(nbins):
     y_ref = h_ref.GetBinContent(i+1)
     y     = h.GetBinContent(i+1)
     dy    = h.GetBinError(i+1)

     if y_ref == 0.:
        continue

     h_div.SetBinContent( i+1, y/y_ref )
     h_div.SetBinError( i+1, dy/y_ref )

  return h_div

#########################################################

def TH1F2TGraph(h):
  g = TGraphErrors()
  g.SetName( "g_%s" % h.GetName() )

  Nbins = h.GetNbinsX()
  for i in range(Nbins):
    x = h.GetBinCenter( i+1 )
    y = h.GetBinContent( i+1 )
    bw = h.GetBinWidth( i+1 )
    dy = h.GetBinError( i+1 )
    dx = bw/2.

    g.SetPoint( i, x, y )
    g.SetPointError( i, dx, dy )

  return g

#########################################################

production = sys.argv[1]
channel = sys.argv[2]
region = sys.argv[3]

infilename_data = str(production+"/"+channel+"/data_JpT10GeV.Zjet_balance."+region+".root")
infilename_mc   = str(production+"/"+channel+"/mc.Zjets_JpT10GeV.Zjet_balance."+region+".root")

infile_data = TFile.Open( infilename_data )
infile_mc   = TFile.Open( infilename_mc )

if infile_data == None:
  print "ERROR: invalid input data file"
  exit(1)
if infile_mc == None:
  print "ERROR: invalid input mc file"
  exit(1)

obs = "j1_pt"
if len(sys.argv) > 1: obs = sys.argv[4]

hname = "balance_vs_%s" % obs
print "INFO: input histogram:", hname

h2_data = infile_data.Get(hname)
h2_mc   = infile_mc.Get(hname)

if h2_data == None:
   print "ERROR: invalid input data histogram"
   exit(1)
if h2_mc == None:
   print "ERROR: invalid input mc histogram"
   exit(1)


h_data = h2_data.ProfileX("h_data").ProjectionX()
h_mc   = h2_mc.ProfileX("h_mc" ).ProjectionX()

SetTH1FStyle( h_data, color=kBlack, markerstyle=20 )
SetTH1FStyle( h_mc,   color=kRed,   markerstyle=24 )

h_data.SetMaximum(1.5)
h_data.SetMinimum(0.75)
h_data.GetYaxis().SetTitle( "<p_{T}(j_{1}) / p_{T}^{ref}>" )
h_data.GetYaxis().SetLabelSize( 0.04 )

c, pad0, pad1 = MakeCanvas()

pad0.cd()

h_data.Draw( "ep" )
h_mc.Draw( "ep same" )

lparams = {
        'xoffset' : 0.60,
        'yoffset' : 0.90,
        'width'   : 0.35,
        'height'  : 0.04,
        }

leg = MakeLegend( lparams )
leg.SetTextFont( 42 )
leg.SetNColumns(1)
leg.AddEntry( h_data, "Data", "ep" )
if channel == "Zmumu":
    leg.AddEntry( h_mc, "Z#rightarrow #mu^{+}#mu^{-}" , "ep" )
if channel == "Zee":
    leg.AddEntry( h_mc, "Z#rightarrow e^{+}e^{-}", "ep" )
leg.Draw()
leg.SetY1( leg.GetY1() - lparams['height'] * leg.GetNRows() )

txt = TLatex()
txt.SetTextFont(42)
txt.SetNDC()
txt.SetTextSize(0.05)
txt.DrawLatex(0.25, 0.85, "ATLAS Internal" )
txt.SetTextSize(0.04)
txt.DrawLatex(0.25, 0.80, "#sqrt{s} = 5.02 TeV, 260 pb^{-1}" )
if region == "lpT20.jpT10":
    txt.DrawLatex(0.25, 0.75, "l_pT>20, j_pT>10" )
if region == "lpT20.jpT20":
    txt.DrawLatex(0.25, 0.75, "l_pT>20, j_pT>20" )
if region == "lpT25.jpT20":
    txt.DrawLatex(0.25, 0.75, "l_pT>25, j_pT>20" )
if region == "lpT25.jpT25":
    txt.DrawLatex(0.25, 0.75, "l_pT>25, j_pT>25" )

xmin = h_data.GetXaxis().GetXmin()
xmax = h_data.GetXaxis().GetXmax()
l = TLine()
l.SetLineStyle(kDashed)
l.SetLineWidth(2)
l.DrawLine(xmin, 1.0, xmax, 1.0)

gPad.RedrawAxis()

pad1.cd()

xtitle = h_data.GetXaxis().GetTitle()

h_ratio_data = DivideBy( h_data,  h_data )
#h_ratio_mc = DivideBy( h_mc,    h_data )
h_ratio_mc = DivideBy( h_data, h_mc)

g_unc = TH1F2TGraph( h_ratio_data )
g_mc  = TH1F2TGraph( h_ratio_mc )

SetTH1FStyle( g_unc, fillstyle=1001, fillcolor=kGray+1, linewidth=0, markersize=0 )
SetTH1FStyle( g_mc,  color=kRed, linewidth=3, markerstyle=h_mc.GetMarkerStyle() )

xmin = h_data.GetXaxis().GetXmin()
xmax = h_data.GetXaxis().GetXmax()
#frame = gPad.DrawFrame( xmin, 0.7, xmax, 1.3 )
frame = gPad.DrawFrame( xmin, 0.9, xmax, 1.1 )
if obs=="pTref":
    frame = gPad.DrawFrame( xmin, 0.93, xmax, 1.07 )

frame.GetXaxis().SetNdivisions(508)
frame.GetYaxis().SetNdivisions(504)
frame.GetXaxis().SetLabelSize( 0.16 )
frame.GetXaxis().SetTitleSize( 0.16 )
frame.GetXaxis().SetTitleOffset( 1.2 )
frame.GetYaxis().SetLabelSize( 0.10 )
#frame.GetYaxis().SetTitle( "#frac{Prediction}{Data}" )
frame.GetYaxis().SetTitle( "#frac{Data}{Prediction}" )
frame.GetYaxis().SetTitleSize( 0.16 )
frame.GetYaxis().SetTitleOffset( 0.5 )
frame.GetXaxis().SetTitle( xtitle )

r_line = TLine()
r_line.SetLineStyle(kDashed)
r_line.SetLineWidth(2)

frame.Draw()
g_unc.Draw( "e2 same" )
r_line.DrawLine( xmin, 1.0, xmax, 1.0 )
g_mc.Draw( "ep same" )

gPad.RedrawAxis()

c.cd()

c.SaveAs( "%s/%s/img/%s/Zjets_balance_%s.png" % (production,channel,region,obs) )
