import os , sys

production = sys.argv[1] # choose one of the following: output_A78 , output_A110_tightLH, output_A110_mediumLH
channel = sys.argv[2]    # choose one of the following: Zee, Zmumu


#region_list = ["lpT20.jpT20","lpT25.jpT20","lpT25.jpT25"]
region_list = ["lpT20.jpT10"]
cp_histo_list = ['cutflow', 'mu', 'vtxz', 'Z_m', 'Z_pt', 'jets_n', 'lep_pt', 'Zj_dPhi', 'j1_pt', 'j1_y', 'j1_eta', 'j2_pt', 'j2_y', 'j2_eta', 'j3_pt', 'j3_y', 'j3_eta', 'j4_pt', 'j4_y', 'j4_eta', 'j1j2_dPhi', 'j1j2_m', 'pT_ref', 'balance', 'balance_low_pTref', 'balance_mid_pTref', 'balance_high_pTref', 'r', 'r_vs_j1_pt', 'r_vs_j1_y', 'r_vs_j1_eta']


bl_histo_list = ["j1_pt", "j1_pt", "j1_y", "j1_eta", "mu", "pTref"]



for region in region_list:
    for histo in cp_histo_list:
        os.system(str('python plot_Zjet_balance_cp.py '+production+' '+channel+' '+region+' '+histo))
    for histo in bl_histo_list:
        os.system(str('python plot_Zjet_balance.py '+production+' '+channel+' '+region+' '+histo))
