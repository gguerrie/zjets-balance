#!/usr/bin/env python

import sys

#from common import *
from common_mediumLH import *

from ROOT import *
from array import array

GeV = 1e3
m_Z_PDG = 91.2*GeV


filelistname = sys.argv[1]
_channel = sys.argv[2]
_lep_pt_cut = sys.argv[3]
_jet_pt_cut = sys.argv[4]

lep_pt_cut= float(_lep_pt_cut)*GeV
jet_pt_cut= float(_jet_pt_cut)*GeV


print('INFO: --- lep_pt_cut: '+str(lep_pt_cut))
print('INFO: --- jet_pt_cut: '+str(jet_pt_cut))
# Read in event tree
treename = "nominal"

# Get ROOT trees
tree = TChain( treename, treename )
for fname in open( filelistname, 'r' ).readlines():
   fname = fname.strip()
   print("INFO: looping over file: " + str(fname))
   tree.Add( fname )

ext = filelistname.split('/')[-1].split('.')[-1]
ofilename = "output_A110_mediumLH/"+_channel+"/" + filelistname.split('/')[-1].replace( ext, "Zjet_balance.lpT"+_lep_pt_cut+".jpT"+_jet_pt_cut+".root")
ofile = TFile.Open( ofilename, "RECREATE" )

# Book histograms
_h = {}
_h['cutflow'] = TH1F( "cutflow", "Cutflow", 4, 0.5, 4.5 )
_h['mu']   = TH1F( "mu", ";<#mu>", 5, -0.5, 4.5 )
_h['vtxz']   = TH1F( "vtxz", ";z_{vtx} [mm]", 50, -150, 150 )
_h['Z_m']  = TH1F( "Z_m",  ";Z mass [GeV]", 30, 60., 120. )
_h['Z_pt'] = TH1F( "Z_pt", ";p_{T}(Z) [GeV]", 30, 0., 300. )
_h['jets_n']  = TH1F( "jets_n", ";Jets multiplicity", 4, 0.5, 4.5 )
_h['lep_pt']  = TH1F( "lep_pt", ";Lepton p_{T} [GeV]", 30, 0., 300. )
_h['Zj_dPhi'] = TH1F( "Zj_dPhi", ";|#Delta#phi(Z,j_{1})|", 32, 0., 3.2 )
#----------
binLowE = [10,15,20,25,30,35,40,45,50,60,70,80,100,120,150,200,250]
NBINS = len(binLowE)-1
#_h['j1_pt']   = TH1F( "j1_pt", ";1st leading jet p_{T} [GeV]", 30, 0., 250. )
_h['j1_pt']   = TH1F( "j1_pt", ";1st leading jet p_{T} [GeV]", NBINS, array('d',binLowE))
#----------
_h['j1_y']    = TH1F( "j1_y",  ";1st leading jet rapidity", 25, 0., 2.5 )
_h['j1_eta']    = TH1F( "j1_eta",  ";1st leading jet #eta", 25, 0., 2.5 )
_h['j2_pt']   = TH1F( "j2_pt", ";2nd leading jet p_{T} [GeV]", 30, 0., 300. )
_h['j2_y']    = TH1F( "j2_y",  ";2nd leading jet rapidity", 20, 0., 2.5 )
_h['j2_eta']    = TH1F( "j2_eta",  ";2nd leading jet #eta", 20, 0., 2.5 )
_h['j3_pt']   = TH1F( "j3_pt", ";3rd leading jet p_{T} [GeV]", 20, 20., 100. )
_h['j3_y']    = TH1F( "j3_y",  ";3rd leading jet rapidity", 10, 0., 2.5 )
_h['j3_eta']    = TH1F( "j3_eta",  ";3rd leading jet #eta", 10, 0., 2.5 )
_h['j4_pt']   = TH1F( "j4_pt", ";4th leading jet p_{T} [GeV]", 15, 20., 80. )
_h['j4_y']    = TH1F( "j4_y",  ";4th leading jet rapidity", 5, 0., 2.5 )
_h['j4_eta']    = TH1F( "j4_eta",  ";4th leading jet #eta", 5, 0., 2.5 )
_h['j1j2_dPhi'] = TH1F( "j1j2_dPhi", ";|#Delta#phi(j_{1}, j_{2})|", 32, 0., 3.2 )
_h['j1j2_m']    = TH1F( "j1j2_m", ";m(j_{1}, j_{2}) [GeV]", 15, 0., 300. )
#-------------

#_h['pT_ref']  = TH1F( "pT_ref", ";p_{T}^{ref} [GeV]", 50, 0., 250. )
_h['pT_ref']  = TH1F( "pT_ref", ";p_{T}^{ref} [GeV]", NBINS, array('d',binLowE))
#-------------
_h['balance'] = TH1F( "balance", ";p_{T}(j_{1}) / p_{T}^{ref}", 30, 0., 3. )
_h['balance_low_pTref']  = TH1F( "balance_low_pTref", ";p_{T}(j_{1}) / p_{T}^{ref}", 30, 0., 3. )
_h['balance_mid_pTref']  = TH1F( "balance_mid_pTref", ";p_{T}(j_{1}) / p_{T}^{ref}", 30, 0., 3. )
_h['balance_high_pTref'] = TH1F( "balance_high_pTref", ";p_{T}(j_{1}) / p_{T}^{ref}", 30, 0., 3. )
_h['r']       = TH1F( "r", ";p_{T}(j_{1})/p_{T}(Z)", 60, 0., 3. )
_h['r_vs_j1_pt'] = TH2F( "r_vs_j1_pt", ";p_{T}(j_{1});p_{T}(j_{1})/p_{T}(Z)", 25, 0., 200., 60, 0., 3. )
_h['r_vs_j1_y']  = TH2F( "r_vs_j1_y",  ";y(j_{1});p_{T}(j_{1})/p_{T}(Z)", 25, 0, 2.5, 60, 0., 3. )
_h['r_vs_j1_eta']  = TH2F( "r_vs_j1_eta",  ";#eta(j_{1});p_{T}(j_{1})/p_{T}(Z)", 25, 0, 2.5, 60, 0., 3. )
#-----------------------
#_h['balance_vs_j1_pt'] = TH2F( "balance_vs_j1_pt", ";p_{T}(j_{1});<p_{T}(j_{1})/p_{T}^{ref}>", 20, 0., 200., 60, 0., 3. )
#_h['balance_vs_j1_y']  = TH2F( "balance_vs_j1_y",  ";y(j_{1});<p_{T}(j_{1})/p_{T}^{ref}>", 15, 0., 2.5, 60, 0., 3. )
#_h['balance_vs_mu']    = TH2F( "balance_vs_mu",    ";<#mu>;<p_{T}(j_{1})/p_{T}^{ref}>", 5, -0.5, 4.5, 60, 0., 3. )
#_h['balance_vs_pTref'] = TH2F( "balance_vs_pTref", ";p_{T}^{ref};<p_{T}(j_{1})/p_{T}^{ref}>", 15, 0., 100., 60, 0., 3. )

_h['balance_vs_j1_pt'] = TH2F( "balance_vs_j1_pt", ";p_{T}(j_{1});<p_{T}(j_{1})/p_{T}^{ref}>", NBINS, array('d',binLowE), 60, 0., 3. )
_h['balance_vs_j1_y']  = TH2F( "balance_vs_j1_y",  ";y(j_{1});<p_{T}(j_{1})/p_{T}^{ref}>", 15, 0., 2.5, 60, 0., 3. )
_h['balance_vs_j1_eta']  = TH2F( "balance_vs_j1_eta",  ";#eta(j_{1});<p_{T}(j_{1})/p_{T}^{ref}>", 15, 0., 2.5, 60, 0., 3. )
_h['balance_vs_mu']    = TH2F( "balance_vs_mu",    ";<#mu>;<p_{T}(j_{1})/p_{T}^{ref}>", 5, -0.5, 4.5, 60, 0., 3. )
_h['balance_vs_pTref'] = TH2F( "balance_vs_pTref", ";p_{T}^{ref};<p_{T}(j_{1})/p_{T}^{ref}>", NBINS, array('d',binLowE), 60, 0., 3. )
#-----------------------

for h in _h.values(): h.Sumw2()

# Define cut table
n_cuts = _h['cutflow'].GetNbinsX()
_h['cutflow'].GetXaxis().SetBinLabel( 1, "Preselection" )
_h['cutflow'].GetXaxis().SetBinLabel( 2, "Z mass window" )
_h['cutflow'].GetXaxis().SetBinLabel( 3, "Z+1j" )
_h['cutflow'].GetXaxis().SetBinLabel( 4, "Z/j back-to-back")

# Event loop
nentries = tree.GetEntries()
print("INFO: looping over %i entries") % nentries
n_good = 0
for ientry in range(nentries):
   tree.GetEntry( ientry )

   eventNumber = tree.eventNumber
   runNumber   = tree.runNumber
   mcChannelNumber = tree.mcChannelNumber

   if ( nentries < 10 ) or ( (ientry+1) % int(float(nentries)/10.)  == 0 ):
      perc = 100. * ientry / float(nentries)
      print ("INFO: Event %-9i (en = %-10i rn = %-10i ) (%3.0f %%)" )% ( ientry, eventNumber, runNumber, perc )

   w = 1.
   if not mcChannelNumber == 0:
      w *= tree.weight_mc

      dsid = str(tree.mcChannelNumber)
      w_dsid = xs[dsid] / gen_sumw[dsid]
      w *= w_dsid
      w *= iLumi

      w_pileup = tree.weight_pileup
      w_jvt    = tree.weight_jvt
      w_lepSF  = tree.weight_leptonSF

      w *= w_pileup * w_jvt * w_lepSF

   if _channel == 'Zee':
       if tree.passed_ee_1j_incl == 0: continue

   if _channel == 'Zmumu':
        if tree.passed_mumu_1j_incl == 0: continue

   #------- added by MJK -------------------
   if tree.jet_pt[0] < jet_pt_cut: continue

   if _channel == 'Zee':
       if tree.el_pt[0] <lep_pt_cut or tree.el_pt[1] <lep_pt_cut: continue
       if (tree.el_charge[0]>0 and tree.el_charge[1]>0) or (tree.el_charge[0]<0 and tree.el_charge[1]<0): continue

   if _channel == 'Zmumu':
        if tree.mu_pt[0] <lep_pt_cut or tree.mu_pt[1] <lep_pt_cut: continue
        if (tree.mu_charge[0]>0 and tree.mu_charge[1]>0) or (tree.mu_charge[0]<0 and tree.mu_charge[1]<0): continue


   #----------------------------------------

   _h['cutflow'].AddBinContent( 1, w )

   _h['mu'].Fill( tree.mu, w )

   l1 = TLorentzVector()
   l2 = TLorentzVector()

   if _channel == 'Zee':
       l1.SetPtEtaPhiE( tree.el_pt[0], tree.el_eta[0], tree.el_phi[0], tree.el_e[0] )
       l2.SetPtEtaPhiE( tree.el_pt[1], tree.el_eta[1], tree.el_phi[1], tree.el_e[1] )

   if _channel == 'Zmumu':
       l1.SetPtEtaPhiE( tree.mu_pt[0], tree.mu_eta[0], tree.mu_phi[0], tree.mu_e[0] )
       l2.SetPtEtaPhiE( tree.mu_pt[1], tree.mu_eta[1], tree.mu_phi[1], tree.mu_e[1] )

#   l1.SetPtEtaPhiE( tree.el_pt[0], tree.el_eta[0], tree.el_phi[0], tree.el_e[0] )
#   l2.SetPtEtaPhiE( tree.el_pt[1], tree.el_eta[1], tree.el_phi[1], tree.el_e[1] )

   Z = l1 + l2

   _h['Z_m'].Fill( Z.M()/GeV, w )
   _h['lep_pt'].Fill( l1.Pt()/GeV, w )
   _h['lep_pt'].Fill( l2.Pt()/GeV, w )

   jets_n = len( tree.jet_pt )
   _h['jets_n'].Fill( jets_n, w )


   if abs( Z.M() - m_Z_PDG ) > 10*GeV: continue
   _h['cutflow'].AddBinContent(	2, w )

   _h['Z_pt'].Fill( Z.Pt()/GeV, w )

   _h['vtxz'].Fill( tree.vtxz, w )

   j1 = TLorentzVector()
   j2 = TLorentzVector()
   j3 = TLorentzVector()
   j4 = TLorentzVector()

   # assume there is always at least 1 jet
   j1.SetPtEtaPhiE( tree.jet_pt[0], tree.jet_eta[0], tree.jet_phi[0], tree.jet_e[0] )

   _h['j1_pt'].Fill( j1.Pt()/GeV, w )
   _h['j1_y'].Fill( abs(j1.Rapidity()), w )
   _h['j1_eta'].Fill( abs(j1.Eta()), w )

   if jets_n > 1:
      j2.SetPtEtaPhiE( tree.jet_pt[1], tree.jet_eta[1], tree.jet_phi[1], tree.jet_e[1] )
      _h['j2_pt'].Fill( j2.Pt()/GeV, w )
      _h['j2_y'].Fill( abs(j2.Rapidity()), w )
      _h['j2_eta'].Fill( abs(j2.Eta()), w )

      jj = j1 + j2
      jj_dPhi = abs( j1.DeltaPhi(j2) )
      _h['j1j2_m'].Fill( jj.M()/GeV, w )
      _h['j1j2_dPhi'].Fill( jj_dPhi, w )
   if jets_n > 2:
      j3.SetPtEtaPhiE( tree.jet_pt[2], tree.jet_eta[2], tree.jet_phi[2], tree.jet_e[2] )
      _h['j3_pt'].Fill( j3.Pt()/GeV, w )
      _h['j3_y'].Fill( abs(j3.Rapidity()), w )
      _h['j3_eta'].Fill( abs(j3.Eta()), w )
   if jets_n > 3:
      j4.SetPtEtaPhiE( tree.jet_pt[3], tree.jet_eta[3], tree.jet_phi[3], tree.jet_e[3] )
      _h['j4_pt'].Fill( j4.Pt()/GeV, w )
      _h['j4_y'].Fill( (j4.Rapidity()), w )
      _h['j4_eta'].Fill( (j4.Eta()), w )

   dPhi_Zj = Z.DeltaPhi( j1 )

   _h['Zj_dPhi'].Fill( abs(dPhi_Zj), w )

   # @Ovidiu:  pT_ref = pT_Z * |cos(dPhi (j1, Z))|
   pT_ref = Z.Pt() * abs( TMath.Cos(dPhi_Zj) )
   balance = j1.Pt()/pT_ref

   #--------- moved cuts 3 and 4 before filling pT_ref and balance
   if jets_n > 1:
     if j2.Pt() > max( [ 10.*GeV, 0.1*pT_ref ] ): continue
   _h['cutflow'].AddBinContent( 3, w )

   if abs(dPhi_Zj) < 2.8: continue
   _h['cutflow'].AddBinContent(	4, w )
   #--------------

   _h['pT_ref'].Fill( pT_ref/GeV, w )
   _h['balance'].Fill( balance, w )
   if (pT_ref>15*GeV) and (pT_ref<20*GeV): _h['balance_low_pTref'].Fill( balance, w )
   if (pT_ref>60*GeV) and (pT_ref<80*GeV): _h['balance_mid_pTref'].Fill( balance, w )
   if (pT_ref>100*GeV):                    _h['balance_high_pTref'].Fill( balance, w )
   '''
   if jets_n > 1:
     if j2.Pt() > max( [ 10.*GeV, 0.1*pT_ref ] ): continue
   _h['cutflow'].AddBinContent( 3, w )

   if abs(dPhi_Zj) < 2.8: continue
   _h['cutflow'].AddBinContent(	4, w )
   '''
   r = j1.Pt() / Z.Pt()

   _h['r'].Fill( r, w )
   _h['r_vs_j1_pt'].Fill( j1.Pt()/GeV, r, w )
   _h['r_vs_j1_y'].Fill( j1.Rapidity(), r, w )
   _h['r_vs_j1_eta'].Fill( j1.Eta(), r, w )

   _h['balance_vs_j1_pt'].Fill( j1.Pt()/GeV, balance, w )
   _h['balance_vs_j1_y'].Fill( abs(j1.Rapidity()), balance, w )
   _h['balance_vs_j1_eta'].Fill( abs(j1.Eta()), balance, w )
   _h['balance_vs_mu'].Fill( tree.mu, balance, w )
   _h['balance_vs_pTref'].Fill( pT_ref/GeV, balance, w )

   n_good += 1

# final stats
if n_good > 0:
  f_good = 100.*n_good / float(nentries)
  print("INFO: events passing final selection: %i / %i (%.1f%%)" % (n_good, nentries, f_good ))
else:
  print("INFO: events passing final selection: 0 (0%%)")

print("INFO: cutflow:")
for i in range(n_cuts):
   print("%-20s :: %i" % ( _h['cutflow'].GetXaxis().GetBinLabel(i+1), _h['cutflow'].GetBinContent(i+1) ))

# Write out
ofile.Write()
ofile.Close()
print("INFO: save output file:", ofile.GetName())
