import numpy as np
from common import *
from ROOT import *
from math import cos, sin, sqrt, pow

from features import *

#####################


def GetEventWeight( tree, syst="nominal" ):
    w = 1.

    w_leptonSF = tree.weight_leptonSF
    w_pileup   = tree.weight_pileup
    w_jvt      = tree.weight_jvt
    w_btag     = tree.weight_bTagSF_MV2c10_77
    w_others   = 1.
    isOtherSyst = True

    if syst == "nominal":
        pass

    elif syst in [ "pileup_UP", "pileup_DOWN" ]:
        w_pileup = tree.weight_pileup_UP if syst == "pileup_UP" else tree.weight_pileup_DOWN

    elif syst in [ "jvt_UP", "jvt_DOWN" ]:
        w_jvt = tree.weight_jvt_UP if syst == "jvt_UP" else tree.weight_jvt_DOWN

    #bTagSF_MV2c10_77_eigenvars_B_down_0
    #bTagSF_MV2c10_77_extrapolation_up
    elif syst.startswith("bTagSF"):
        if "eigenvars" in syst:
            k = int( syst.split('_')[-1] )
            syst_btag   = syst.replace( "_up_%i"%k, "_up" ).replace( "_down_%i"%k, "_down" )
            syst_branch = "weight_%s" % syst_btag
            exec( "w_btag = tree.%s[%i]" % (syst_branch, k ) )
        else:
            syst_branch = "weight_%s" % syst	
            exec( "w_btag = tree.%s" % syst_branch )

    else:
        if syst in systematics_weight:
            syst_branch = "weight_%s" % syst
            exec( "w_others = tree.%s" % syst_branch )

    w *= w_leptonSF * w_pileup * w_jvt * w_btag * w_others

    return w


#####################


def HadronicW_mass( jets ):
    W_had = TLorentzVector()
    W_had.dR = 10.
    W_had.j1_index = -1
    W_had.j2_index = -1

    dM_W = 1e10
    jets_n = len(jets)
    for a in range(jets_n):
        j1 = jets[a]
        for b in range(a+1, jets_n):
            j2 = jets[b]

            jj = j1 + j2
            dM_jj = abs(jj.M() - m_W_PDG)

            if dM_jj > dM_W: continue

            W_had = TLorentzVector(jj)
            W_had.dR = j1.DeltaR(j2)
            W_had.j1_index = a
            W_had.j2_index = b
            dM_W  = abs(W_had.M() - m_W_PDG)

    return W_had

#~~~~~~~~~~~~~~~~~~

def HadronicW( jets ):
    W_had = TLorentzVector()
    W_had.dR = 10.
    W_had.j1_index = -1
    W_had.j2_index = -1

    jets_n = len(jets)
    for a in range(jets_n):
        j1 = jets[a]
        if j1.isBtagged == True: continue

        for b in range(a+1, jets_n):
            j2 = jets[b]
            if j2.isBtagged == True: continue

            dR = j1.DeltaR(j2)
            if dR > W_had.dR: continue

            W_had = j1 + j2
            W_had.dR = dR
            W_had.j1_index = a
            W_had.j2_index = b

    return W_had

#~~~~~~~~~~~~~~~~~~

def HadronicW_fullreco( jets ):

    jj = TLorentzVector()
    jj.dR = 10.

    jj.j1_index = -1
    jj.j2_index = -1

#    jets.sort( key=lambda jet: jet.mv2c10, reverse=True )

    jets_n = len( jets )
    jets_lf = []
    jets_hf = []
    for j in jets:
        #bw = j.mv2c10

        if j.isBtagged:
            if len(jets_hf)<2:
                jets_hf += [ j ]
            else:
                jets_lf += [ j ]
        else:
            jets_lf += [ j ]

    jets_hf_n = len(jets_hf)
    jets_lf_n = len(jets_lf)

    if jets_lf_n < 2:
        return jj

    for a in range(jets_lf_n):
        j1 = jets_lf[a]

        for b in range(a+1, jets_lf_n):
            j2 = jets_lf[b]

            dR = j1.DeltaR( j2 )
            if dR > jj.dR: continue

            jj = j1 + j2
            jj.dR = dR
            jj.j1_index = j1.index
            jj.j2_index = j2.index

    jj.dR = min( 10., jj.dR )

#    jets.sort( key=lambda jet: jet.Pt(), reverse=True )

    return jj


########################


def HadronicTop( Whad, bjets ):
    jjb = TLorentzVector()
    jjb.dR_Wb  = 100.
    jjb.z_Wb   = 0.
    for bj in bjets:
        if bj.index in [ Whad.j1_index, Whad.j2_index ]: continue

        dR = Whad.DeltaR( bj )
        if dR > jjb.dR_Wb: continue

        jjb = Whad + bj
        jjb.dR_Wb = dR
        jjb.z_Wb = Whad.Pt() / bj.Pt()

    jjb.dR_Wb = min( jjb.dR_Wb, 10. )

    return jjb

#~~~~~~~~~~~~~~~~~~~~~

def TTbarReconstruction_4j2b( W_lep, W_had, jets, bjets ):

    bjets_n = len(bjets)

    t_lep = TLorentzVector()
    t_lep.dR_Wb = 1e10
    t_lep.bj_index = -1
    t_lep.z_Wb = 1e10

    for bj in bjets:
        if bj.index in [ W_had.j1_index, W_had.j2_index ]: continue

        dR = W_lep.DeltaR( bj )
        if dR > t_lep.dR_Wb: continue

        t_lep          = bj + W_lep
        t_lep.dR_Wb    = dR
        t_lep.bj_index = bj.index
        t_lep.z_Wb     = W_lep.Pt() / bj.Pt()

    t_had = TLorentzVector()
    t_had.dR_Wb = 1e10
    t_had.z_Wb  = 1e10

    for bj in bjets:
        if bj.index == t_lep.bj_index: continue
        if bj.index in [ W_had.j1_index, W_had.j2_index ]: continue

        dR = W_had.DeltaR( bj )
        if dR > t_had.dR_Wb: continue

        t_had      = W_had + bj
        t_had.bj_index = bj.index
        t_had.dR_Wb = dR
        t_had.z_Wb  = W_had.Pt() / bj.Pt()

    ttbar = t_had + t_lep

    # calc ttbar observables

    # cosThetaStar
    v_tt = ttbar.Vect()
    v_boost = -ttbar.BoostVector()

    t_had_star = TLorentzVector( t_had )
    t_had_star.Boost( v_boost.X(), v_boost.Y(), v_boost.Z() )
    t_had.cosThetaStar = t_had_star.CosTheta()

    t_lep_star = TLorentzVector( t_lep )
    t_lep_star.Boost( v_boost.X(), v_boost.Y(), v_boost.Z() )
    t_lep.cosThetaStar = t_lep_star.CosTheta()

    return t_had, t_lep, ttbar

#~~~~~~~~~~~~~~~~~~~~~

def TTbarReconstruction_4j1b( W_lep, W_had, jets, bjets ):
    jets_n = len(jets)
    bj = bjets[0]

    dR_lep = W_lep.DeltaR( bj )
    dR_had = W_had.DeltaR( bj )

    t_lep = TLorentzVector()
    t_had = TLorentzVector()

    t_had.dR_Wb = 1e10
    t_had.z_Wb  = -1.
    t_lep.dR_Wb = 1e10
    t_lep.z_Wb  = -1.

    if dR_lep < dR_had:
        t_lep = W_lep + bj
        t_lep.bj_index = bj.index
        t_lep.dR_Wb = W_lep.DeltaR(bj)
        t_lep.z_Wb  = W_lep.Pt() / bj.Pt()

        for j in jets:
            if j.index == W_had.j1_index: continue
            if j.index == W_had.j2_index: continue

            dR = W_had.DeltaR(j)
            if dR > t_had.dR_Wb: continue

            t_had = W_had + j
            t_had.bj_index = j.index
            t_had.dR_Wb = dR
            t_had.z_Wb  = W_had.Pt() / j.Pt()
    else:
        t_had = W_had + bj
        t_had.bj_index = bj.index
        t_had.dR_Wb = W_had.DeltaR(bj)
        t_had.z_Wb  = W_had.Pt() / bj.Pt()

        for j in jets:
            if j.index == W_had.j1_index: continue
            if j.index == W_had.j2_index: continue

            dR = W_lep.DeltaR(j)

            if dR > t_lep.dR_Wb: continue

            t_lep = W_lep + j
            t_lep.bj_index = j.index
            t_lep.dR_Wb = dR
            t_lep.z_Wb  = W_lep.Pt() / j.Pt()

    ttbar = t_had + t_lep

    # calc ttbar observables

    # cosThetaStar
    v_tt = ttbar.Vect()
    v_boost = -ttbar.BoostVector()

    t_had_star = TLorentzVector( t_had )
    t_had_star.Boost( v_boost.X(), v_boost.Y(), v_boost.Z() )
    t_had.cosThetaStar = t_had_star.CosTheta()

    t_lep_star = TLorentzVector( t_lep )
    t_lep_star.Boost( v_boost.X(), v_boost.Y(), v_boost.Z() )
    t_lep.cosThetaStar = t_lep_star.CosTheta()

    return t_had, t_lep, ttbar

#~~~~~~~~~~~~~~~~~~~~~

def TTbarReconstruction_4j0b( W_lep, W_had, jets ):

    t_lep = TLorentzVector()
    t_lep.dR_Wb = 1e10
    t_lep.bj_index = -1
    t_lep.z_Wb = 1e10

    for j in jets:

        dR = W_lep.DeltaR( j )
        if dR > t_lep.dR_Wb: continue

        t_lep          = j + W_lep
        t_lep.dR_Wb    = dR
        t_lep.bj_index = j.index
        t_lep.z_Wb     = W_lep.Pt() / j.Pt()

    t_had = TLorentzVector()
    t_had.dR_Wb = 1e10
    t_had.bj_index = -1
    t_had.z_Wb = 1e10
    for j in jets:
        if j.index == t_lep.bj_index: continue

        dR = W_had.DeltaR( j )
        if dR > t_had.dR_Wb: continue

        t_had          = j + W_had
        t_had.dR_Wb    = dR
        t_had.bj_index = j.index
        t_had.z_Wb     = W_had.Pt() / j.Pt()

    ttbar = t_had + t_lep

    # calc ttbar observables

    # cosThetaStar
    v_tt = ttbar.Vect()
    v_boost = -ttbar.BoostVector()

    t_had_star = TLorentzVector( t_had )
    t_had_star.Boost( v_boost.X(), v_boost.Y(), v_boost.Z() )
    t_had.cosThetaStar = t_had_star.CosTheta()

    t_lep_star = TLorentzVector( t_lep )
    t_lep_star.Boost( v_boost.X(), v_boost.Y(), v_boost.Z() )
    t_lep.cosThetaStar = t_lep_star.CosTheta()

    return t_had, t_lep, ttbar

#~~~~~~~~~~~~~~~~~~~~~

def CalcTTbarChi2( t_had, t_lep, W_had ):
    sigma_m_top = 30*GeV
    sigma_m_W   = 15*GeV

    t_had_m     = t_had.M()
    t_lep_m     = t_lep.M()
    W_had_m     = W_had.M()

    dM_t_had    = t_had_m-m_top_PDG
    dM_t_lep    = t_lep_m-m_top_PDG
    dM_W_had    = W_had_m-m_W_PDG

    ttbar_chi2 = dM_t_had*dM_t_had/(sigma_m_top*sigma_m_top) + \
                 dM_t_lep*dM_t_lep/(sigma_m_top*sigma_m_top) + \
                 dM_W_had*dM_W_had/(sigma_m_W*sigma_m_W)

    return ttbar_chi2

########## KLFitter implementation ######################
def KLF_MakeNeutrino( tree):
    klf_nu = TLorentzVector()
    klf_nu.SetPtEtaPhiE( tree.klfitter_model_nu_pt[0], tree.klfitter_model_nu_eta[0], tree.klfitter_model_nu_phi[0], tree.klfitter_model_nu_E[0])
    return klf_nu

def KLF_Reconstruction(lep,nu,jets, tree):
    #klf_nu = KLF_MakeNeutrino( tree)
    klf_nu = nu

    t_lep = TLorentzVector()
    t_lep = lep + klf_nu + jets[tree.klfitter_model_blep_jetIndex[0]]

    W_had = TLorentzVector()
    W_had.dR = 10.
    W_had = jets[tree.klfitter_model_lq1_jetIndex[0]] + jets[tree.klfitter_model_lq2_jetIndex[0]]
    W_dR=jets[tree.klfitter_model_lq1_jetIndex[0]].DeltaR(jets[tree.klfitter_model_lq2_jetIndex[0]])
    W_had.dR =W_dR

    t_had = TLorentzVector()
    t_had.dR = 100.
    t_had = jets[tree.klfitter_model_bhad_jetIndex[0]] + W_had
    t_dR=jets[tree.klfitter_model_bhad_jetIndex[0]].DeltaR(W_had)
    t_had.dR =t_dR

    ttbar = t_had + t_lep

    return W_had, t_lep, t_had, ttbar

########################
def CalcJJMasses( jets ):
    all_pairs = []
    jets_n = len(jets)
    for i in range(jets_n):
        for k in range( i+1, jets_n ):
            j1 = jets[i]
            j2 = jets[k]
            jj = j1 + j2
            all_pairs += [ jj.M() ]

    all_pairs.sort()
    return all_pairs

#~~~~~~~~~~~~~

def CalcLJMasses( lep, jets ):
    all_pairs = []
    jets_n = len(jets)
    for i in range(jets_n):
        lj = lep + jets[i]
        all_pairs += [ lj.M() ]
    all_pairs.sort()
    return all_pairs

#~~~~~~~~~~~~~

def CalcMinDPhi_lj( lep, jets ):
   min_dPhi = 10.
   jets_n = len(jets)
   for i in range(jets_n):
     j = jets[i]
     dPhi = lep.DeltaPhi(j)
     if abs(dPhi) > abs(min_dPhi): continue

     min_dPhi = dPhi

   return min_dPhi

########################

def NeutrinoPz( lep, met_px, met_py ):
#   met_px = met_met * TMath.Cos(met_phi)
#   met_py = met_met * TMath.Sin(met_phi)

   alpha = m_W_PDG*m_W_PDG - lep.M()*lep.M() + 2. * ( met_px*lep.Px() + met_py*lep.Py() )

   a = lep.Pz()*lep.Pz() - lep.E()*lep.E()
   b = alpha * lep.Pz()
   c = alpha/4. + lep.E() * ( met_px*met_px + met_py*met_py )

   delta = b*b - 4*a*c
   if delta > 0:
      pz_plus  = ( -b + TMath.Sqrt( delta ) ) / ( 2.*a )
      pz_minus = ( -b - TMath.Sqrt( delta ) ) / ( 2.*a )
      return pz_plus if abs(pz_plus) < abs(pz_minus) else pz_minus
   else:
      return -b/(2.*a)

#~~~~~~~~~


def MakeNeutrino( met_met, met_phi, lep ):
   nu_px = met_met * TMath.Cos(met_phi)
   nu_py = met_met * TMath.Sin(met_phi)
   nu_pz = NeutrinoPz( lep, nu_px, nu_py )
   nu_E = TMath.Sqrt( nu_px*nu_px + nu_py*nu_py + nu_pz*nu_pz ) # massless!

   nu = TLorentzVector()
   nu.SetPxPyPzE( nu_px, nu_py, nu_pz, nu_E )
   return nu


########################



def EventShapeVariables( all_fso ):
   D = 0.
   for p in all_fso:
     D += p.P()*p.P()

   S = TMatrixD(3,3)
   for p in all_fso:
     p3 = [ p.Px(), p.Py(), p.Pz() ]

     for i in range(3):
       for j in range(3):
         S[i][j] += p3[i] * p3[j] / D

   S_eigen = TMatrixDEigen(S)
   S_evals = S_eigen.GetEigenValuesRe()
   l1 = S_evals[0]
   l2 = S_evals[1]
   l3 = S_evals[2]

   sphericity_T = 2.* l2 / ( l1 + l2 )
   aplanarity   = 1.5 * l3

   return aplanarity, sphericity_T

   #return { 'aplanarity' : Aplanarity, 'sphericity_T' : SphericityT }

#####################

def MakeEventElectron( tree ):
    lep = TLorentzVector()
    lep.SetPtEtaPhiE( tree.el_pt[0], tree.el_cl_eta[0], tree.el_phi[0], tree.el_e[0] )
    lep.q = tree.el_charge[0]
    lep.flav = 11 * lep.q
    lep.topoetcone = tree.el_topoetcone20[0]
    lep.ptvarcone  = tree.el_ptvarcone20[0]
    lep.d0sig      = tree.el_d0sig[0]
    return lep

#~~~~~~~~~~~~

def MakeEventMuon( tree ):
    lep = TLorentzVector()
    lep.SetPtEtaPhiE( tree.mu_pt[0], tree.mu_eta[0], tree.mu_phi[0], tree.mu_e[0] )
    lep.q = tree.mu_charge[0]
    lep.flav = 13 * lep.q
    lep.topoetcone = tree.mu_topoetcone20[0]
    lep.ptvarcone  = tree.mu_ptvarcone30[0]
    lep.d0sig      = tree.mu_d0sig[0]
    return lep

#~~~~~~~~~~~~~~~~~~

def MakeEventJets( tree, b_tag_cut=0.713 ):
    jets = []
    bjets = []

    jets_n = len( tree.jet_pt )
    for i in range(jets_n):
        jets += [ TLorentzVector() ]
        jets[-1].SetPtEtaPhiE( tree.jet_pt[i], tree.jet_eta[i], tree.jet_phi[i], tree.jet_e[i] )
        jets[-1].index = i
        jets[-1].ntrk = max( 0, tree.jet_QGTaggerNTrack[i] )
 
        jets[-1].mv2c10 = tree.jet_mv2c10[i]
        jets[-1].isBtagged = False
        if jets[-1].mv2c10 > b_tag_cut:
            jets[-1].isBtagged = True
            bjets += [ TLorentzVector(jets[-1]) ]
            bjets[-1].mv2c10 = jets[-1].mv2c10
            bjets[-1].index = i
            bjets[-1].ntrk = jets[-1].ntrk

    return jets, bjets

#####################

def RotateEvent( lep, jets, bjets, phi=0., flip_eta=False ):

    lep_new            = TLorentzVector( lep )
    lep_new.q          = lep.q
    lep_new.flav       = lep.flav
    lep_new.topoetcone = lep.topoetcone
    lep_new.ptvarcone  = lep.ptvarcone
    lep_new.d0sig      = lep.d0sig

    lep_new.RotateZ( phi )
    if flip_eta:
      lep_new.SetPtEtaPhiE( lep.Pt(), -lep.Eta(), lep.Phi(), lep.E() )

    jets_new = []
    for j in jets:
        jets_new += [ TLorentzVector(j) ]
        j_new = jets_new[-1]

        j_new.ntrk   = j.ntrk
        j_new.mv2c10 = j.mv2c10
        j_new.index  = j.index

        j_new.RotateZ( phi )
        if flip_eta:
           j_new.SetPtEtaPhiE( j_new.Pt(), -j_new.Eta(), j_new.Phi(), j_new.E() )

    bjets_new = []
    for bj in bjets:
       bjets_new += [ TLorentzVector(bj) ]
       bj_new = bjets_new[-1]

       bj_new.ntrk   = bj.ntrk
       bj_new.mv2c10 = bj.mv2c10
       bj_new.index  = bj.index

       bj_new.RotateZ( phi )
       if flip_eta:
          bj_new.SetPtEtaPhiE( bj_new.Pt(), -bj_new.Eta(), bj_new.Phi(), bj_new.E() )

    return lep_new, jets_new, bjets_new


#####################

def make_rnn_input( lep, jets ):
    event = np.zeros( [ n_fso_max, n_features_rnn_PxPyPzE ] )

    event[0][0] = lep.Px() / GeV
    event[0][1] = lep.Py() / GeV
    event[0][2] = lep.Pz() / GeV
#    event[0][3] = lep.E()  / GeV
    event[0][3] = lep.q
#    event[0][5] = lep.d0sig

    jets_n = len(jets)
    jets_n_max = min( n_fso_max-1, jets_n )

    for i in range( jets_n_max ):
        event[i+1][0] = jets[i].Px() / GeV
        event[i+1][1] = jets[i].Py() / GeV
        event[i+1][2] = jets[i].Pz() / GeV
 #       event[i+1][3] = jets[i].E( ) / GeV
        event[i+1][3] = jets[i].mv2c10
 #       event[i+1][5] = jets[i].ntrk

    # linearize: sequence of ops is: scale->reshape->classify
    event = event.reshape( ( n_fso_max*n_features_rnn_PxPyPzE ) )

    return event

#~~~~~~~~~~~~~~~~~~~~

def make_mlp_input( met_met, met_phi, lep, jets, bjets ):
    jets_n  = len(jets)
    bjets_n = len(bjets)

    jets50_n = 0
    for j in jets:
        if j.Pt() > 50*GeV: jets50_n += 1

    mTW = TMath.Sqrt( 2. * lep.Pt() * met_met * ( 1. - TMath.Cos( lep.Phi() - met_phi ) ) )

    nu     = MakeNeutrino( met_met, met_phi, lep )
    W_lep  = lep + nu
    W_had  = HadronicW( jets ) # min_dR(j,j)

    all_jj_m = CalcJJMasses( jets )
    all_lj_m = CalcLJMasses( lep, jets )
    min_dPhi_lj = CalcMinDPhi_lj( lep, jets )

    aplanarity, sphericity_T = EventShapeVariables( [ lep, nu ] + jets )

    #if bjets_n >= 2:
    #    t_had, t_lep, ttbar = TTbarReconstruction_4j2b( W_lep, W_had, jets, bjets )
    #elif bjets_n == 1:
    #    t_had, t_lep, ttbar = TTbarReconstruction_4j1b( W_lep, W_had, jets, bjets )
    #else:
    #    t_had, t_lep, ttbar = TTbarReconstruction_4j0b( W_lep, W_had, jets )


    # based on arXiv:1712.06857
 #   event = np.zeros( 15 )
 #   event[0] = jets_n
 #   event[1] = jets50_n
 #   event[2] = bjets_n
 #   event[3] = all_jj_m[0] / GeV
 #   event[4] = t_had.cosThetaStar
 #   event[5] = t_lep.M() / GeV
 #   event[6] = aplanarity
 #   event[7] = t_had.M() / GeV
 #   event[8] = all_lj_m[0] / GeV
 #   event[9] = all_jj_m[1] / GeV

    event = np.zeros( 23 )
    event[0]  = jets_n
    event[1]  = jets50_n
    event[2]  = lep.Pt() / GeV
    event[3]  = lep.Eta()
    event[4]  = lep.d0sig
    event[5]  = lep.topoetcone / GeV
    event[6]  = lep.ptvarcone / GeV
    event[7]  = min_dPhi_lj
    event[8]  = met_met / GeV
    event[9]  = mTW / GeV
    event[10]  = aplanarity
    event[11]  = sphericity_T
    event[12] = jets[0].Pt() / GeV
    event[13] = jets[1].Pt() / GeV
    event[14] = jets[2].Pt() / GeV    if jets_n > 2 else -1.
    event[15] = jets[0].Eta()
    event[16] = jets[1].Eta()
    event[17] = jets[2].Eta()  if jets_n > 2 else -10.
    event[18] = jets[0].mv2c10
    event[19] = jets[1].mv2c10
    event[20] = jets[2].mv2c10 if jets_n > 2 else -10.
    event[21] = W_had.dR
    event[22] = W_had.M() / GeV


#    event = np.zeros( 19 )
#    event[0]  = met_met / GeV
#    event[1]  = lep.Pt() / GeV
#    event[2]  = mTW_lep / GeV
#    event[3]  = abs( lep.Eta() )
#    event[4]  = jets[0].Pt() / GeV
#    event[5]  = jets[1].Pt() / GeV
#    event[6]  = jets[2].Pt() / GeV
#    event[7]  = abs( jets[0].Eta() )
#    event[8]  = abs( jets[1].Eta() )
#    event[9]  = abs( jets[2].Eta() )
#    event[10]  = jets[0].mv2c10
#    event[11]  = jets[1].mv2c10
#    event[12]  = jets[2].mv2c10
#    event[13] = hadW.dR
#    event[14] = hadW.M() / GeV
#    event[15] = hadt.dR_Wb
#    event[16] = hadt.z_Wb
#    event[17] = jets_n
#    event[18] = jets50_n

    return event
#------------------------- For using KLFitter outputs in training ------------
def make_mlp_input_klf( tree, lep, jets, bjets ):
    met_met=tree.met_met
    met_phi=tree.met_phi
    jets_n  = len(jets)
    bjets_n = len(bjets)

    jets50_n = 0
    for j in jets:
        if j.Pt() > 50*GeV: jets50_n += 1

    mTW = TMath.Sqrt( 2. * lep.Pt() * met_met * ( 1. - TMath.Cos( lep.Phi() - met_phi ) ) )

    nu     = MakeNeutrino( met_met, met_phi, lep )
    W_lep  = lep + nu
    W_had  = HadronicW( jets ) # min_dR(j,j)

    reconstructable_event = False
    if(jets_n >= 4 and len(tree.klfitter_logLikelihood)!=0): reconstructable_event=True

    if (reconstructable_event):
        klf_W_had, klf_t_lep, klf_t_had, klf_ttbar= KLF_Reconstruction(lep,nu,jets, tree)
        klf_logLikelihood=tree.klfitter_logLikelihood[0]
    else:
        klf_logLikelihood=0

    all_jj_m = CalcJJMasses( jets )
    all_lj_m = CalcLJMasses( lep, jets )
    min_dPhi_lj = CalcMinDPhi_lj( lep, jets )

    aplanarity, sphericity_T = EventShapeVariables( [ lep, nu ] + jets )

    event = np.zeros( 24 )
    event[0]  = jets_n
    event[1]  = jets50_n
    event[2]  = lep.Pt() / GeV
    event[3]  = lep.Eta()
    event[4]  = lep.d0sig
    event[5]  = lep.topoetcone / GeV
    event[6]  = lep.ptvarcone / GeV
    event[7]  = min_dPhi_lj
    event[8]  = met_met / GeV
    event[9]  = mTW / GeV
    event[10]  = aplanarity
    event[11]  = sphericity_T
    event[12] = jets[0].Pt() / GeV
    event[13] = jets[1].Pt() / GeV
    event[14] = jets[2].Pt() / GeV    if jets_n > 2 else -1.
    event[15] = jets[0].Eta()
    event[16] = jets[1].Eta()
    event[17] = jets[2].Eta()  if jets_n > 2 else -10.
    event[18] = jets[0].mv2c10
    event[19] = jets[1].mv2c10
    event[20] = jets[2].mv2c10 if jets_n > 2 else -10.
    event[21] = klf_W_had.dR if reconstructable_event else W_had.dR
    event[22] = klf_W_had.M() / GeV if reconstructable_event else W_had.M() / GeV
    #event[23] = klf_t_had.dR if reconstructable_event else t_had.dR
    #event[24] = klf_t_had.M() / GeV if reconstructable_event else t_had.M()
    event[23] = klf_logLikelihood

    return event
########################
