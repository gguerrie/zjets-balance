from ROOT import *
from array import array

# 5.02TeV data taken in November 2017, calibrated luminosity 256.827 pb
# with an uncertainty of 1.6 percent (tag OflLumi-5TeV-003),

iLumi = 256.827

nb = 1e3
pb = 1.
fb = 1e-3

###### Cross-section values from  https://cds.cern.ch/record/2657141

xs = {
  # Diboson
  '361063' : 4.624*pb, # Sherpa_CT10_llll
  '361064' : 532.370*fb, # Sherpa_CT10_lllvSFMinus
  '361065' : 1041.0*fb, # Sherpa_CT10_lllvOFMinus
  '361066' : 843.280*fb, # Sherpa_CT10_lllvSFPlus
  '361067' : 1.633*pb, # Sherpa_CT10_lllvOFPlus
  '361068' : 3.356*pb, # Sherpa_CT10_llvv.
  '361091' : 6.059*pb, # Sherpa_CT10_WplvWmqq_SHv21_improved
  '361092' : 6.082*pb, # Sherpa_CT10_WpqqWmlv_SHv21_improved.
  '361093' : 2.503*pb, # Sherpa_CT10_WlvZqq_SHv21_improved
  '361094' : 751.84*fb, # Sherpa_CT10_WqqZll_SHv21_improved
  '361096' : 3.789*0.148*pb, # Sherpa_CT10_ZqqZll_SHv21_improved < LEONID updated >

  # W/Z+jets
#   '364381' : 739.2, # 721.79*pb, # Sherpa_225_NNPDF30NNLO_Zee
#   '364382' : 739.2, # 722.89*pb, # Sherpa_225_NNPDF30NNLO_Zmumu
#   '364383' : 739.2, # 722.89*pb, # Sherpa_225_NNPDF30NNLO_Ztautau
#   '364384' : 7259.0, # 7190.0*pb, # Sherpa_225_NNPDF30NNLO_Wenu
#   '364385' : 7259.0, # 7187.0*pb, # Sherpa_225_NNPDF30NNLO_Wmunu
#   '364386' : 7259.0, # 7183.0*pb, # Sherpa_225_NNPDF30NNLO_Wtaunu
   #Zjets
   '700320': 55.5413852 * 1.0*pb, #sherpa
   '700321': 286.392209 * 1.0*pb, #sherpa
   '700322': 1879.375291 * 1.0*pb, #sherpa
   '700323': 54.1530727 * 1.0*pb, #sherpa
   '700324': 287.347368 * 1.0*pb, #sherpa
   '700325': 1879.997255 * 1.0*pb, #sherpa
   '700326': 6.68473707 * 1.0*pb, #sherpa
   '700327': 34.5071089 * 1.0*pb, #sherpa
   '700328': 234.1268655 * 1.0*pb, #sherpa
   '700329': 24.803136 * 1.0*pb, #sherpa
   '700330': 127.00462 * 1.0*pb, #sherpa
   '700331': 861.442904 * 1.0*pb, #sherpa
   '700332': 23.11928376 * 1.0*pb, #sherpa
   '700333': 117.1817198 * 1.0*pb, #sherpa
   '700334': 792.6878012 * 1.0*pb, #sherpa


  # Top
  '410470' : 68.9*0.544*pb, # PhPy8EG_A14_ttbar_hdamp258p75_nonallhad (NOMINAL)  < LEONID updated >
  #'410480' : *pb, # PhPy8EG_A14_ttbar_hdamp517p5_SingleLep >> CAREFUL, this is SINGLE LEP ONLY, while nominal is nonallhad
  #'410557' : *pb, # PowhegHerwig7EvtGen_H7UE_tt_hdamp258p75_704_SingleLep

  # Single top
  '410644' : 539.950*fb, # PowhegPythia8EvtGen_A14_singletop_schan_lept_top
  '410645' : 275.12*fb, # PowhegPythia8EvtGen_A14_singletop_schan_lept_antitop
  '410646' : 2.990*pb, # PowhegPythia8EvtGen_A14_Wt_DR_inclusive_top
  '410647' : 2.983*pb, # PowhegPythia8EvtGen_A14_Wt_DR_inclusive_antitop
  #'410648' : 315.03*fb, # PowhegPythia8EvtGen_A14_Wt_DR_dilepton_top
  #'410649' : 314.28*fb, # PowhegPythia8EvtGen_A14_Wt_DR_dilepton_antitop
  #'410654' : 3.159*pb,  # PowhegPythia8EvtGen_A14_Wt_DS_inclusive_top
  #'410655' : 3.445*pb,  # PowhegPythia8EvtGen_A14_Wt_DS_inclusive_antitop
  #'410656' : 332.81*fb, # PowhegPythia8EvtGen_A14_Wt_DS_dilepton_top
  #'410657' : 362.67*fb, # PowhegPythia8EvtGen_A14_Wt_DS_dilepton_antitop
  '410658' : 5.414*pb,  # PhPy8EG_A14_tchan_BW50_lept_top
  '410659' : 2.682*pb, # PhPy8EG_A14_tchan_BW50_lept_antitop
}

gen_sumw = {
'361063' : 12987.265729, #
'361064' : 6675.736204, #
'361065' : 10602.797001, #
'361066' : 9415.902838, #
'361067' : 14948.667654, #
'361068' : 49442.097456, #
'361091' : 18272.450520, #
'361092' : 32161.492550, #
'361093' : 9638.816951, #
'361094' : 1282.537476, #
'361096' : 5135.123139, #
'700329' : 5174636767.259277, #
'364381' : 3942132.79688, # --- from output_A110_mediumLH/JpT10GeV
'364382' : 3948798.83984, # --- from output_A110_mediumLH/JpT10GeV
'364383' : 1185015.17969, # --- from output_A110_mediumLH/JpT10GeV
'364384' : 19795913.657600, #
'364385' : 19805577.161479, #
'364386' : 4747537.420197, #
'410464' : 172086723.787575, #
'410470' : 178062394.875, # --- from output_A110_mediumLH
'410480' : 175778973.062805, #
'410557' : 175544408.752457, #
'410644' : 27009.638704, #
'410645' : 13769.261930, #
'410646' : 149583.423963, #
'410647' : 149250.855294, #
'410648' : 15445.619652, #
'410649' : 15721.674819, #
'410654' : 148638.853269, #
'410655' : 165884.109080, #
'410656' : 15653.902725, #
'410657' : 17323.050155, #
'410658' : 270840.341531, #
'410659' : 134187.132417, #
}

GeV = 1e3
TeV = 1e6

m_W_PDG   = 80.4*GeV
m_top_PDG = 172.5*GeV

#####################

systematics_tree = [
   "nominal",
   "EG_RESOLUTION_ALL__1down",
   "EG_RESOLUTION_ALL__1up",
   "EG_SCALE_AF2__1down",
   "EG_SCALE_AF2__1up",
   "EG_SCALE_ALL__1down",
   "EG_SCALE_ALL__1up",
   "JET_CategoryReduction_JET_BJES_Response__1down",
   "JET_CategoryReduction_JET_BJES_Response__1up",
   "JET_CategoryReduction_JET_EffectiveNP_Detector1__1down",
   "JET_CategoryReduction_JET_EffectiveNP_Detector1__1up",
   "JET_CategoryReduction_JET_EffectiveNP_Mixed1__1down",
   "JET_CategoryReduction_JET_EffectiveNP_Mixed1__1up",
   "JET_CategoryReduction_JET_EffectiveNP_Mixed2__1down",
   "JET_CategoryReduction_JET_EffectiveNP_Mixed2__1up",
   "JET_CategoryReduction_JET_EffectiveNP_Mixed3__1down",
   "JET_CategoryReduction_JET_EffectiveNP_Mixed3__1up",
   "JET_CategoryReduction_JET_EffectiveNP_Modelling1__1down",
   "JET_CategoryReduction_JET_EffectiveNP_Modelling1__1up",
   "JET_CategoryReduction_JET_EffectiveNP_Modelling2__1down",
   "JET_CategoryReduction_JET_EffectiveNP_Modelling2__1up",
   "JET_CategoryReduction_JET_EffectiveNP_Modelling3__1down",
   "JET_CategoryReduction_JET_EffectiveNP_Modelling3__1up",
   "JET_CategoryReduction_JET_EffectiveNP_Modelling4__1down",
   "JET_CategoryReduction_JET_EffectiveNP_Modelling4__1up",
   "JET_CategoryReduction_JET_EffectiveNP_Statistical1__1down",
   "JET_CategoryReduction_JET_EffectiveNP_Statistical1__1up",
   "JET_CategoryReduction_JET_EffectiveNP_Statistical2__1down",
   "JET_CategoryReduction_JET_EffectiveNP_Statistical2__1up",
   "JET_CategoryReduction_JET_EffectiveNP_Statistical3__1down",
   "JET_CategoryReduction_JET_EffectiveNP_Statistical3__1up",
   "JET_CategoryReduction_JET_EffectiveNP_Statistical4__1down",
   "JET_CategoryReduction_JET_EffectiveNP_Statistical4__1up",
   "JET_CategoryReduction_JET_EffectiveNP_Statistical5__1down",
   "JET_CategoryReduction_JET_EffectiveNP_Statistical5__1up",
   "JET_CategoryReduction_JET_EffectiveNP_Statistical6__1down",
   "JET_CategoryReduction_JET_EffectiveNP_Statistical6__1up",
   "JET_CategoryReduction_JET_EtaIntercalibration_Modelling__1down",
   "JET_CategoryReduction_JET_EtaIntercalibration_Modelling__1up",
   "JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_highE__1down",
   "JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_highE__1up",
   "JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_negEta__1down",
   "JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_negEta__1up",
   "JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_posEta__1down",
   "JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_posEta__1up",
   "JET_CategoryReduction_JET_EtaIntercalibration_TotalStat__1down",
   "JET_CategoryReduction_JET_EtaIntercalibration_TotalStat__1up",
   "JET_CategoryReduction_JET_Flavor_Composition__1down",
   "JET_CategoryReduction_JET_Flavor_Composition__1up",
   "JET_CategoryReduction_JET_Flavor_Response__1down",
   "JET_CategoryReduction_JET_Flavor_Response__1up",
   "JET_CategoryReduction_JET_Pileup_OffsetMu__1down",
   "JET_CategoryReduction_JET_Pileup_OffsetMu__1up",
   "JET_CategoryReduction_JET_Pileup_OffsetNPV__1down",
   "JET_CategoryReduction_JET_Pileup_OffsetNPV__1up",
   "JET_CategoryReduction_JET_Pileup_PtTerm__1down",
   "JET_CategoryReduction_JET_Pileup_PtTerm__1up",
   "JET_CategoryReduction_JET_Pileup_RhoTopology__1down",
   "JET_CategoryReduction_JET_Pileup_RhoTopology__1up",
   "JET_CategoryReduction_JET_PunchThrough_MC16__1down",
   "JET_CategoryReduction_JET_PunchThrough_MC16__1up",
   "JET_CategoryReduction_JET_SingleParticle_HighPt__1down",
   "JET_CategoryReduction_JET_SingleParticle_HighPt__1up",
   "JET_JER_SINGLE_NP__1up",
   "MET_SoftTrk_ResoPara",
   "MET_SoftTrk_ResoPerp",
   "MET_SoftTrk_ScaleDown",
   "MET_SoftTrk_ScaleUp",
   "MUON_ID__1down",
   "MUON_ID__1up",
   "MUON_MS__1down",
   "MUON_MS__1up",
   "MUON_SAGITTA_RESBIAS__1down",
   "MUON_SAGITTA_RESBIAS__1up",
   "MUON_SAGITTA_RHO__1down",
   "MUON_SAGITTA_RHO__1up",
   "MUON_SCALE__1down",
   "MUON_SCALE__1up",
]

systematics_btagging = [
   "bTagSF_MV2c10_77_eigenvars_B_down",
   "bTagSF_MV2c10_77_eigenvars_B_up",
   "bTagSF_MV2c10_77_eigenvars_C_down",
   "bTagSF_MV2c10_77_eigenvars_C_up",
   "bTagSF_MV2c10_77_eigenvars_Light_down",
   "bTagSF_MV2c10_77_eigenvars_Light_up",
   "bTagSF_MV2c10_77_extrapolation_down",
   "bTagSF_MV2c10_77_extrapolation_up",
   "bTagSF_MV2c10_77_extrapolation_from_charm_down",
   "bTagSF_MV2c10_77_extrapolation_from_charm_up",
]

systematics_weight = [
   "indiv_SF_EL_ChargeID",
   "indiv_SF_EL_ChargeID_DOWN",
   "indiv_SF_EL_ChargeID_UP",
   "indiv_SF_EL_ChargeMisID",
   "indiv_SF_EL_ChargeMisID_STAT_DOWN",
   "indiv_SF_EL_ChargeMisID_STAT_UP",
   "indiv_SF_EL_ChargeMisID_SYST_DOWN",
   "indiv_SF_EL_ChargeMisID_SYST_UP",
   "indiv_SF_EL_ID_DOWN",
   "indiv_SF_EL_ID_UP",
   "indiv_SF_EL_Isol_DOWN",
   "indiv_SF_EL_Isol_UP",
   "indiv_SF_EL_Reco_DOWN",
   "indiv_SF_EL_Reco_UP",
   "indiv_SF_EL_Trigger_DOWN",
   "indiv_SF_EL_Trigger_UP",
   "indiv_SF_MU_ID_STAT_DOWN",
   "indiv_SF_MU_ID_STAT_LOWPT_DOWN",
   "indiv_SF_MU_ID_STAT_LOWPT_UP",
   "indiv_SF_MU_ID_STAT_UP",
   "indiv_SF_MU_ID_SYST_DOWN",
   "indiv_SF_MU_ID_SYST_LOWPT_DOWN",
   "indiv_SF_MU_ID_SYST_LOWPT_UP",
   "indiv_SF_MU_ID_SYST_UP",
   "indiv_SF_MU_Isol_STAT_DOWN",
   "indiv_SF_MU_Isol_STAT_UP",
   "indiv_SF_MU_Isol_SYST_DOWN",
   "indiv_SF_MU_Isol_SYST_UP",
   "indiv_SF_MU_TTVA_STAT_DOWN",
   "indiv_SF_MU_TTVA_STAT_UP",
   "indiv_SF_MU_TTVA_SYST_DOWN",
   "indiv_SF_MU_TTVA_SYST_UP",
   "indiv_SF_MU_Trigger_STAT_DOWN",
   "indiv_SF_MU_Trigger_STAT_UP",
   "indiv_SF_MU_Trigger_SYST_DOWN",
   "indiv_SF_MU_Trigger_SYST_UP",
   "jvt_DOWN",
   "jvt_UP",
   "leptonSF_EL_SF_ID_DOWN",
   "leptonSF_EL_SF_ID_UP",
   "leptonSF_EL_SF_Isol_DOWN",
   "leptonSF_EL_SF_Isol_UP",
   "leptonSF_EL_SF_Reco_DOWN",
   "leptonSF_EL_SF_Reco_UP",
   "leptonSF_EL_SF_Trigger_DOWN",
   "leptonSF_EL_SF_Trigger_UP",
   "leptonSF_MU_SF_ID_STAT_DOWN",
   "leptonSF_MU_SF_ID_STAT_LOWPT_DOWN",
   "leptonSF_MU_SF_ID_STAT_LOWPT_UP",
   "leptonSF_MU_SF_ID_STAT_UP",
   "leptonSF_MU_SF_ID_SYST_DOWN",
   "leptonSF_MU_SF_ID_SYST_LOWPT_DOWN",
   "leptonSF_MU_SF_ID_SYST_LOWPT_UP",
   "leptonSF_MU_SF_ID_SYST_UP",
   "leptonSF_MU_SF_Isol_STAT_DOWN",
   "leptonSF_MU_SF_Isol_STAT_UP",
   "leptonSF_MU_SF_Isol_SYST_DOWN",
   "leptonSF_MU_SF_Isol_SYST_UP",
   "leptonSF_MU_SF_TTVA_STAT_DOWN",
   "leptonSF_MU_SF_TTVA_STAT_UP",
   "leptonSF_MU_SF_TTVA_SYST_DOWN",
   "leptonSF_MU_SF_TTVA_SYST_UP",
   "leptonSF_MU_SF_Trigger_STAT_DOWN",
   "leptonSF_MU_SF_Trigger_STAT_UP",
   "leptonSF_MU_SF_Trigger_SYST_DOWN",
   "leptonSF_MU_SF_Trigger_SYST_UP",
   "pileup_DOWN",
   "pileup_UP",
   ]


#####################


#known_channels = [ "ljets_", "ejets_", "mujets_" ]

#known_regions = [ "3jetex0bex", "3jetex1bex", "3jetex2bex", "3jetex0bin",
#		  "4jetex0bex", "4jetex1bex", "4jetex2bex", "4jetex0bin",
#		  "4jetin0bex", "4jetin1bex", "4jetin2bex", "4jetin0bin",
#		  "4jetin1bin", "4jetin2bin",
#		  "5jetin0bex", "5jetin1bex", "5jetin2bex", "5jetin0bin",
#		  "5jetin1bin", "5jetin2bin",
#                  "3jetex0bex_SR", "3jetex1bex_SR", "3jetex2bex_SR", "3jetex0bin_SR",
#                  "4jetex0bex_SR", "4jetex1bex_SR", "4jetex2bex_SR", "4jetex0bin_SR",
#                  "4jetin0bex_SR", "4jetin1bex_SR", "4jetin2bex_SR", "4jetin0bin_SR",
#                  "4jetin1bin_SR", "4jetin2bin_SR",
#                  "5jetin0bex_SR", "5jetin1bex_SR", "5jetin2bex_SR", "5jetin0bin_SR",
#                  "5jetin1bin_SR", "5jetin2bin_SR"
#		  ]

known_channels = [ "ejets", "mujets"]

known_regions = ["2jetex0bex", "2jetex1bex", "2jetex2bex",
                 "3jetex0bex", "3jetex1bex", "3jetex2bex", "3jetex3bex",
                 "4jetex0bex", "4jetex1bex", "4jetex2bex", "4jetex3bin",
                 "5jetin0bex", "5jetin1bex", "5jetin2bex", "5jetin3bin"
		 ]



histograms = {}
for channel in known_channels:
   histograms[channel] = {}

   for region in known_regions:
      histograms[channel][region] = {}

#      histograms[channel][region]['mu']         = TH1F( '%s_%s_mu'%(channel,region), ";<#mu>",  6, -0.5, 5.5 )

#      histograms[channel][region]['el_charge']  = TH1F( '%s_%s_el_charge'%(channel,region),  ";Electron charge", 3, -1.5, 1.5 )
#      histograms[channel][region]['mu_charge']  = TH1F( '%s_%s_mu_charge'%(channel,region),  ";Muon charge",     3, -1.5, 1.5 )
#      histograms[channel][region]['lep_charge'] = TH1F( '%s_%s_lep_charge'%(channel,region), ";Lepton charge",   3, -1.5, 1.5 )

      histograms[channel][region]['lep_pt']   = TH1F( "%s_%s_lep_pt"%(channel,region), ";Lepton p_{T} [GeV]", 15, 0., 300. )
#      histograms[channel][region]['lep_eta']  = TH1F( "%s_%s_lep_eta"%(channel,region), ";Lepton #eta", 15, -3.0, 3.0 )
#      histograms[channel][region]['lep_d0sig'] = TH1F( "%s_%s_lep_d0sig"%(channel,region), ";Lepton d0 sig", 15, -3, 3 )
#      histograms[channel][region]['lep_min_dPhi_lj'] = TH1F( "%s_%s_lep_min_dPhi_lj"%(channel,region), ";min #Delta#phi(l,j)", 16, -3.2, 3.2 )
#      histograms[channel][region]['lep_topoetcone']  = TH1F( "%s_%s_lep_topoetcone"%(channel,region), ";Topocluster E_{T} cone [GeV]", 20, -1., 4. )
#      histograms[channel][region]['lep_ptvarcone']   = TH1F( "%s_%s_lep_ptvarcone"%(channel,region), ";Variable-R p_{T} cone [GeV]", 15, 0.5, 3.5 )

#      histograms[channel][region]['jets_n']   = TH1F( "%s_%s_jets_n"%(channel,region),  ";Jets multiplicity", 8, 0.5, 8.5 )

#      histograms[channel][region]['jets_pt']   = TH1F( "%s_%s_jets_pt"%(channel,region), ";Jets p_{T} [GeV]", 15, 0., 300. )
      histograms[channel][region]['j1_pt']   = TH1F( "%s_%s_j1_pt"%(channel,region), ";1st jet p_{T} [GeV]", 15, 0., 300. )
#      histograms[channel][region]['j2_pt']   = TH1F( "%s_%s_j2_pt"%(channel,region), ";2nd jet p_{T} [GeV]", 15, 0., 300. )
#      histograms[channel][region]['j3_pt']   = TH1F( "%s_%s_j3_pt"%(channel,region), ";3rd jet p_{T} [GeV]", 10, 0., 200. )
#      histograms[channel][region]['j4_pt']   = TH1F( "%s_%s_j4_pt"%(channel,region), ";4th jet p_{T} [GeV]", 10, 0., 200. )
#      histograms[channel][region]['j5_pt']   = TH1F( "%s_%s_j5_pt"%(channel,region), ";5th jet p_{T} [GeV]", 10, 0., 200. )

#      histograms[channel][region]['jets_eta'] = TH1F( "%s_%s_jets_eta"%(channel,region),  ";Jets #eta", 15, -3.0, 3.0 )
#      histograms[channel][region]['j1_eta']   = TH1F( "%s_%s_j1_eta"%(channel,region),    ";1st jet #eta", 15, -3.0, 3.0 )
#      histograms[channel][region]['j2_eta']   = TH1F( "%s_%s_j2_eta"%(channel,region),    ";2nd jet #eta", 15, -3.0, 3.0 )
#      histograms[channel][region]['j3_eta']   = TH1F( "%s_%s_j3_eta"%(channel,region),    ";3rd jet #eta", 15, -3.0, 3.0 )
#      histograms[channel][region]['j4_eta']   = TH1F( "%s_%s_j4_eta"%(channel,region),    ";4th jet #eta", 15, -3.0, 3.0 )
 ##     histograms[channel][region]['j5_eta']   = TH1F( "%s_%s_j5_eta"%(channel,region),    ";5th jet #eta", 15, -3.0, 3.0 )

#      histograms[channel][region]['jets_MV2c10'] = TH1F( "%s_%s_jets_MV2c10"%(channel,region),  ";Jets MV2c10", 20, -1., 1. )
#      histograms[channel][region]['j1_MV2c10'] = TH1F( "%s_%s_j1_MV2c10"%(channel,region),  ";1st jet MV2c10", 20, -1., 1. )
#      histograms[channel][region]['j2_MV2c10'] = TH1F( "%s_%s_j2_MV2c10"%(channel,region),  ";2nd jet MV2c10", 20, -1., 1. )
#      histograms[channel][region]['j3_MV2c10'] = TH1F( "%s_%s_j3_MV2c10"%(channel,region),  ";3rd jet MV2c10", 20, -1., 1. )
 ##     histograms[channel][region]['j4_MV2c10'] = TH1F( "%s_%s_j4_MV2c10"%(channel,region),  ";4th jet MV2c10", 20, -1., 1. )
 #     histograms[channel][region]['j5_MV2c10'] = TH1F( "%s_%s_j5_MV2c10"%(channel,region),  ";5th jet MV2c10", 20, -1., 1. )

 #     histograms[channel][region]['bjets_n']  = TH1F( "%s_%s_bjets_n"%(channel,region),  ";b-jets multiplicity", 4, -0.5, 3.5 )
  #    histograms[channel][region]['bjets_pt']  = TH1F( "%s_%s_bjets_pt"%(channel,region), ";b-jets p_{T} [GeV]", 15, 0., 300. )
  ##    histograms[channel][region]['bj1_pt']   = TH1F( "%s_%s_bj1_pt"%(channel,region), ";1st b-jet p_{T} [GeV]", 15, 0., 300. )
  #    histograms[channel][region]['bj2_pt']   = TH1F( "%s_%s_bj2_pt"%(channel,region), ";2nd b-jet p_{T} [GeV]", 15, 0., 300. )


      histograms[channel][region]['met']  = TH1F( "%s_%s_met"%(channel,region),  ";E_{T}^{miss} [GeV]", 20, 0., 200. )
  #    histograms[channel][region]['mtw_lep']  = TH1F( "%s_%s_mtw_lep"%(channel,region),  ";m_{T}^{l,v} [GeV]", 20, 0., 200. )
  #    histograms[channel][region]['mtw_had']  = TH1F( "%s_%s_mtw_had"%(channel,region),  ";m_{T}^{j,j} [GeV]", 20, 0., 200. )

   #   histograms[channel][region]['W_had_dR']  = TH1F( "%s_%s_W_had_dR"%(channel,region),  ";W #Delta R(j,j)",   16, 0., 6. )
   #   histograms[channel][region]['W_had_m']   = TH1F( "%s_%s_W_had_m"%(channel,region),   ";m^{W,had} [GeV]",  20, 0., 200. )

   #   histograms[channel][region]['t_had_dR'] = TH1F( "%s_%s_t_had_dR"%(channel,region), ";Hadronic top #Delta R(W,b)", 24, 0., 6. )
   #   histograms[channel][region]['t_had_m']  = TH1F( "%s_%s_t_had_m"%(channel,region),  ";m^{t,had} [GeV]", 15, 50, 350 )

   #   histograms[channel][region]['klf_logLikelihood']  = TH1F( "%s_%s_klf_logLikelihood"%(channel,region),  ";logLikelihood", 10, -100, -30)
   ##   histograms[channel][region]['klf_W_had_m']  = TH1F( "%s_%s_klf_W_had_m"%(channel,region),  ";klf_W_had_m [GeV]", 20, 50, 120)
    #  histograms[channel][region]['klf_W_had_dR']  = TH1F( "%s_%s_klf_W_had_dR"%(channel,region),  ";W #Delta R(j,j)",   16, 0., 6. )
    #  histograms[channel][region]['klf_t_had_m']  = TH1F( "%s_%s_klf_t_had_m"%(channel,region),  ";klf_t_had_m [GeV]", 20, 150, 250)
    #  histograms[channel][region]['klf_t_had_dR'] = TH1F( "%s_%s_klf_t_had_dR"%(channel,region), ";Hadronic top #Delta R(W,b)", 32, 0., 6. )


      for h in histograms[channel][region].values(): h.Sumw2()

  #####################
